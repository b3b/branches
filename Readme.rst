branches
========


Service deploy instructions
---------------------------

To install and run service from a docker registry, fill environment variables file and execute::

  docker run --rm -p 8000:8000 --env-file .env registry.gitlab.com/b3b/branches:latest


Install and run from a repository
---------------------------------

- Execute: ``git clone https://gitlab.com/b3b/branches.git``
- Fill environment variables file
- Execute: ``docker-compose up``

API page will be available on `http://127.0.0.1:8000/api/`.


Install and run for development
-------------------------------

- Execute: ``git clone https://gitlab.com/b3b/branches.git``
- Execute: ``pip install -r requirements-test.txt``
- Install geospatial libraries: ``apt-get install gdal-bin``
- Run tests: ``pytest``
- Run linter: ``pylint branches_task branches``

- Run the application:

  - Fill environment variables file
  - Execute: ``./run``


Environment variables file configuration
----------------------------------------

- Fill file `.env`, for example::

    DEBUG=true
    SECRET_KEY=test
    ALLOWED_HOSTS=127.0.0.1
    INTERNAL_IPS=127.0.0.1
    POSTGRES_USER=admin
    POSTGRES_PASSWORD=password
    POSTGRES_DB=testtask
    DATABASE_URL=postgis://admin:password@db:5432/postgres
    DEFAULT_FILE_STORAGE=storages.backends.s3boto3.S3Boto3Storage
    AWS_ACCESS_KEY_ID=XXX
    AWS_SECRET_ACCESS_KEY=XXX
    AWS_STORAGE_BUCKET_NAME=mybucket
    AWS_S3_REGION_NAME=eu-central-1
