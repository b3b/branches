-r requirements.txt

pylint==2.4.4

pytest==5.3.1
pytest-env==0.6.2
pytest-django==3.7.0
