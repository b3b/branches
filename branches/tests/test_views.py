from django.contrib.gis.geos import Point
from django.urls import reverse
from rest_framework import status
import pytest

from branches.tests.factories import BranchFactory, EmployeeFactory


@pytest.fixture
def employees_collection():
    branches = [
        BranchFactory(name='branch number 1'),
        BranchFactory(name='branch number 2')
    ]
    return [
        EmployeeFactory(first_name='emp 1', last_name='C', position='position 1', branch=branches[0]),
        EmployeeFactory(first_name='emp 2', last_name='B', position='position 3', branch=branches[0]),
        EmployeeFactory(first_name='emp 3', last_name='A', position='position 2', branch=branches[1])
    ]


def test_branches_list_status_code(client, db):
    response = client.get(reverse('branches:branch-list'))
    assert response.status_code == status.HTTP_200_OK


def test_branches_post_not_allowed(client, db):
    response = client.post(reverse('branches:branch-list'))
    assert response.status_code == status.HTTP_405_METHOD_NOT_ALLOWED


def test_branches_list_instance_returned(client, db):
    branch = BranchFactory()
    response = client.get(reverse('branches:branch-list'))
    data = response.json()['results']
    assert len(data) == 1
    assert data[0]['name'] == branch.name


def test_branches_list_fields_returned(client, db):
    BranchFactory(
        name='test branch',
        location=Point(55.26, 36.15, srid=4326),
        facade_image='/media/test.jpg'
    )
    response = client.get(reverse('branches:branch-list'))
    data = response.json()['results']
    assert data[0]['name'] == 'test branch'
    assert data[0]['lng'] == 55.26
    assert data[0]['lat'] == 36.15
    assert '/media/test.jpg' in data[0]['facade_image']
    data[0]['employees'].endswith('/api/employees/?branch__name=test+branch')


def test_employees_list_status_code(client, db):
    response = client.get(reverse('branches:employee-list'))
    assert response.status_code == status.HTTP_200_OK


def test_employees_post_not_allowed(client, db):
    response = client.post(reverse('branches:employee-list'))
    assert response.status_code == status.HTTP_405_METHOD_NOT_ALLOWED


def test_employees_list_instance_returned(client, db):
    employee = EmployeeFactory()
    response = client.get(reverse('branches:employee-list'))
    data = response.json()['results']
    assert len(data) == 1
    assert data[0]['first_name'] == employee.first_name


def test_employees_list_fields_returned(client, db):
    EmployeeFactory(
        first_name='test first name',
        last_name='test last name',
        position='test position'
    )
    response = client.get(reverse('branches:employee-list'))
    data = response.json()['results']
    assert data[0]['first_name'] == 'test first name'
    assert data[0]['last_name'] == 'test last name'
    assert data[0]['position'] == 'test position'


@pytest.mark.parametrize(
    'name, expected', (
        ('branch number 1', ['emp 1', 'emp 2']),
        ('branch number 2', ['emp 3']),
        ('branch number 3', []),
    )
)
def test_employees_list_filter_by_branch_name(client, db, employees_collection, name, expected):
    response = client.get(reverse('branches:employee-list') + f"?branch__name={name}")
    assert response.status_code == status.HTTP_200_OK
    assert sorted([emp['first_name'] for emp in response.json()['results']]) == expected


@pytest.mark.parametrize(
    'ordering, expected', (
        ('first_name', ['emp 1', 'emp 2', 'emp 3']),
        ('-first_name', ['emp 3', 'emp 2', 'emp 1']),
        ('last_name', ['emp 3', 'emp 2', 'emp 1']),
        ('-last_name', ['emp 1', 'emp 2', 'emp 3']),
        ('position', ['emp 1', 'emp 3', 'emp 2']),
        ('-position', ['emp 2', 'emp 3', 'emp 1']),
        ('branch__name,-first_name', ['emp 2', 'emp 1', 'emp 3']),
        ('-branch__name,first_name', ['emp 3', 'emp 1', 'emp 2']),
    )
)
def test_employees_list_order_by_field(client, db, employees_collection, ordering, expected):
    data = client.get(reverse('branches:employee-list') + f"?ordering={ordering}").json()['results']
    assert [emp['first_name'] for emp in data] == expected


@pytest.mark.parametrize(
    'location, expected', (
        ('lng=55.26&lat=36.15', ['branch 1']),
        ('lng=55.26', ['branch 1', 'branch 2']),
        ('lat=36.15', ['branch 1', 'branch 2']),
    )
)
def test_branches_list_find_nearest(client, db, location, expected):
    BranchFactory(
        name='branch 1',
        location=Point(55.26, 36.15, srid=4326),
    )
    BranchFactory(
        name='branch 2',
        location=Point(59.92, -123.58, srid=4326),
    )
    data = client.get(reverse('branches:branch-list') + f"?{location}").json()['results']
    assert [branch['name'] for branch in data] == expected
