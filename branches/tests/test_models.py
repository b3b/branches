from django.contrib.gis.geos import Point
from django.db.utils import IntegrityError
import pytest

from branches.models import Branch
from branches.tests.factories import (
    BranchFactory,
    EmployeeFactory,
)

pytestmark = pytest.mark.django_db  # all tests of this module use DB


def test_branch_created():
    assert BranchFactory().id


def test_branch_multiple_created():
    assert BranchFactory.create_batch(2)


def test_employee_created():
    assert EmployeeFactory().id


def test_employee_multiple_created():
    assert BranchFactory.create_batch(2)


def test_branch_name_is_uniq():
    BranchFactory(name='test')
    with pytest.raises(IntegrityError):
        BranchFactory(name='test')


@pytest.mark.parametrize(
    'longitude, latitude, expected', (
        (55.26, 36.15, 'branch 1'),
        (59.92, -123.58, 'branch 2'),
        (56.26, 0, 'branch 1'),
    )
)
def test_branch_filter_nearest_location(latitude, longitude, expected):
    BranchFactory(
        name='branch 1',
        location=Point(55.26, 36.15, srid=4326),
    )
    BranchFactory(
        name='branch 2',
        location=Point(59.92, -123.58, srid=4326),
    )
    assert Branch.objects.all().filter_nearest_location(longitude, latitude).get().name == expected


def test_branch_filter_nearest_location_on_empty_set():
    assert not Branch.objects.count()
    assert not Branch.objects.all().filter_nearest_location(1, 1).count()
