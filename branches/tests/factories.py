import factory
from django.contrib.gis.geos import Point
from factory.django import DjangoModelFactory


class BranchFactory(DjangoModelFactory):

    class Meta:
        model = 'branches.Branch'

    name = factory.Faker('company')
    location = Point(1, 1, srid=4326)


class EmployeeFactory(DjangoModelFactory):

    class Meta:
        model = 'branches.Employee'

    branch = factory.SubFactory(BranchFactory)
    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
    position = factory.Faker('job')
