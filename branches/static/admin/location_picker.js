$(document).ready(function(){

    var location_is_disabled = $('#map').hasClass('disabled');
    var default_location = new L.LatLng('42.4319794', '19.185839')


    var get_location_input = function() {
        /* Reads current location from inputs */
        var lat = $('#location_latitude').val();
        var lng = $('#location_longitude').val();
        if (lat === "" || lng === "") return null;
        return new L.LatLng(lat, lng);
    };

    var update_location_input = function(latlng) {
        /* Fills inputs */
        $('#location_latitude').val(latlng.lat.toFixed(6));
        $('#location_longitude').val(latlng.lng.toFixed(6));
    };

    var update_location_map = function(latlng) {
        /* Moves map marker */
        if (marker === null) {
            marker = new L.marker(latlng, {draggable: !location_is_disabled});
            marker.addTo(map);
            marker.on('dragend', function() {
                var latlng = marker.getLatLng();
       	        update_location_input(latlng);
                map.panTo(latlng);
            });
        } else {
            marker.setLatLng(latlng); 
        }
        map.panTo(latlng);
    }

    var map = L.map('map', {zoom: 12});

    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
	maxZoom: 18,
	attribution: ''
    }).addTo(map);
    var marker = null;

    if (get_location_input() !== null) {
        map.panTo(get_location_input());
        update_location_map(get_location_input());
    } else {
        map.panTo(default_location);
    }

    if (location_is_disabled) return;

    map.on('click', function(e) {
        update_location_map(e.latlng);
        update_location_input(e.latlng);
    });

    $( ".location_input" ).keyup(function() {
        update_location_map(get_location_input());
    });

});
