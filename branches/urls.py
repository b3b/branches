from django.urls import path

from branches.views import APIIndexView, BranchListView, EmployeeListView

app_name = 'branches'

url_titles = [
    ('Branch List', 'branch-list'),
    ('Employee List', 'employee-list'),
]

urlpatterns = [
    path('', APIIndexView.as_view(), {'url_titles': url_titles}, name='index'),
    path('branches/', BranchListView.as_view(), name='branch-list'),
    path('employees/', EmployeeListView.as_view(), name='employee-list'),
]
