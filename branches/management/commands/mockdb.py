import random

from django.conf import settings
from django.contrib.auth.models import Permission
from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model
from django.contrib.gis.geos import Point

from factory.django import ImageField
from factory.fuzzy import FuzzyChoice
from factory import LazyAttribute

from branches.tests.factories import BranchFactory, EmployeeFactory


class Command(BaseCommand):
    """Mock database with users, branches and employees."""
    help = __doc__

    def handle(self, *args, **options):
        if settings.TESTING:
            # do not execute when running from tests
            return

        print("Create branches")
        branches = BranchFactory.create_batch(
            size=10,
            facade_image=ImageField(),
            location=LazyAttribute(
                lambda obj: Point(
                    random.uniform(-0.683209, 28.1589223),
                    random.uniform(46.053135, 53.5680082)
                )
            )
        )

        print("Create employees")
        EmployeeFactory.create_batch(10000, branch=FuzzyChoice(branches))

        branch_permissions = Permission.objects.filter(content_type__app_label='branches',
                                                       content_type__model='branch')
        employee_permissions = Permission.objects.filter(content_type__app_label='branches',
                                                         content_type__model='employee')
        User = get_user_model()

        print('Create super user admin:branches')
        User.objects.create_superuser(username='admin', password='branches', email='')

        print('Create user editor:branches')
        user = User.objects.create_user(first_name='', last_name='', username='editor', password='branches',
                                        is_staff=True)
        user.user_permissions.add(*employee_permissions)
        user.user_permissions.add(*branch_permissions)

        print('Create user staff:branches')
        user = User.objects.create_user(first_name='', last_name='', username='staff', password='branches',
                                        is_staff=True)
        user.user_permissions.add(*employee_permissions)
        user.user_permissions.add(*branch_permissions.exclude(codename='can_edit_location'))
