from urllib.parse import urlencode

from rest_framework import serializers
from rest_framework.reverse import reverse

from branches.models import Branch, Employee


class EmployeeSerializer(serializers.ModelSerializer):

    class Meta:
        model = Employee
        fields = ('first_name', 'last_name', 'position')


class BranchSerializer(serializers.ModelSerializer):
    lng = serializers.ReadOnlyField(source='longitude')
    lat = serializers.ReadOnlyField(source='latitude')
    employees = serializers.SerializerMethodField()

    class Meta:
        model = Branch
        fields = ('name', 'lng', 'lat', 'facade_image', 'employees')

    def get_employees(self, obj: Branch) -> str:
        """Returns branch employees list URL."""
        url = self.context['request'].build_absolute_uri(reverse(f"branches:employee-list"))
        query = urlencode({'branch__name': obj.name})
        return f"{url}?{query}"
