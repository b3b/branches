from django.contrib import admin
from django.http.request import HttpRequest
from django.contrib.gis.db.models import PointField
from django.forms import ModelForm
from django.utils.html import mark_safe
from django_admin_listfilter_dropdown.filters import DropdownFilter

from branches.models import Employee, Branch
from branches.widgets import LocationPickerWidget


@admin.register(Employee)
class EmployeeAdmin(admin.ModelAdmin):
    list_display = ['last_name', 'first_name', 'position']
    list_filter = [
        ('last_name', DropdownFilter),
        ('first_name', DropdownFilter),
    ]


class NewEmployeeInline(admin.TabularInline):
    model = Employee
    extra = 3
    verbose_name = "new employee"
    verbose_name_plural = "new employees"

    def has_change_permission(self, request: HttpRequest, obj: Employee = None) -> bool:
        return False

    def has_view_permission(self, request: HttpRequest, obj: Employee = None) -> bool:
        return False


@admin.register(Branch)
class BranchAdmin(admin.ModelAdmin):
    fields = ['name', 'facade_image', 'facade_image_preview', 'location']
    readonly_fields = ['facade_image_preview']
    formfield_overrides = {
        PointField: {'widget': LocationPickerWidget()},
    }
    inlines = [NewEmployeeInline]

    def facade_image_preview(self, obj: Branch) -> str:
        """Show preview of facade image field."""
        return mark_safe(f'<img height=100px src="{obj.facade_image.url}" />')

    def get_form(self, request: HttpRequest, obj: Branch = None, change: bool = False, **kwargs) -> ModelForm:
        form = super().get_form(request, obj, change, **kwargs)
        if not request.user.has_perm('branches.can_edit_location'):
            form.base_fields['location'].disabled = True
        return form
