import django_filters

from branches.models import BranchQuerySet


class NearestLocationFilterSet(django_filters.FilterSet):
    lat = django_filters.NumberFilter(field_name='location', label='Latitude')
    lng = django_filters.NumberFilter(field_name='location', label='Longitude')

    def filter_queryset(self, queryset: BranchQuerySet) -> BranchQuerySet:
        """Find the closest branch by coordinates."""
        latitude = self.form.cleaned_data.get('lat', None)
        longitude = self.form.cleaned_data.get('lng', None)
        if latitude is not None and longitude is not None:
            return queryset.filter_nearest_location(longitude=float(longitude), latitude=float(latitude))
        return queryset
