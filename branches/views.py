from collections import OrderedDict
from typing import List, Tuple

from django.urls import reverse
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import OrderingFilter
from rest_framework.generics import ListAPIView
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView

from branches.filters import NearestLocationFilterSet
from branches.models import Branch, Employee
from branches.serializers import BranchSerializer, EmployeeSerializer


class EmployeeListView(ListAPIView):
    """Branches List API, with a filtering by branch name
    and with possibility to sort by first name, last name, position and branch name.
    """
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_fields = ['branch__name']
    ordering_fields = ['last_name', 'first_name', 'position', 'branch__name']


class BranchListView(ListAPIView):
    """Branches List API.
    Use "lat=...&lng=..." filter to find the closest branch by coordinates.
    """
    queryset = Branch.objects.all()
    serializer_class = BranchSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_class = NearestLocationFilterSet


class APIIndexView(APIView):
    """
    Simple Django Application API
    """

    def get(self, request: Request, url_titles: List[Tuple[str, str]]) -> Response:
        """Return list of available enpoints."""
        def get_url(name):
            return request.build_absolute_uri(reverse(f"branches:{name}"))

        return Response(OrderedDict([
            (title, get_url(name)) for title, name in url_titles
        ]))
