from django.contrib.gis.forms.widgets import BaseGeometryWidget


class LocationPickerWidget(BaseGeometryWidget):
    """
    Widget thats allow to fill coordinates in two ways: by typing values in the text boxes
    or by pointing a location on a map.
    """
    template_name = 'admin/location_picker.html'

    class Media:
        css = {
            'all': (
                'leaflet/leaflet.css',
                'admin/location_picker.css',
            )
        }
        js = (
            'jquery-3.4.1.min.js',
            'leaflet/leaflet.js',
            'admin/location_picker.js',
        )

    def get_context(self, name, value, attrs):
        context = super().get_context(name, value, attrs)
        coords = getattr(value, 'coords', None)
        context['longitude'] = coords[0] if coords else ''
        context['latitude'] = coords[1] if coords else ''
        return context

    def value_from_datadict(self, data, files, name):
        try:
            latitude, longitude = data[f"{name}-latitude"], data[f"{name}-longitude"]
        except KeyError:
            return None
        return f"SRID=4326;POINT({longitude} {latitude})"
