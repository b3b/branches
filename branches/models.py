from django.db import models

from django.contrib.gis.db.models import PointField
from django.contrib.gis.db.models.functions import Distance
from django.contrib.gis.geos import Point


class BranchQuerySet(models.query.QuerySet):
    """Office branch model queryset."""

    def filter_nearest_location(self, longitude: float, latitude: float) -> 'BranchQuerySet':
        """Return queryset with a branch that is closest to a given location."""
        return self.annotate(
            distance=Distance('location', Point(longitude, latitude, srid=4326))
        ).order_by('distance')[:1]


class Branch(models.Model):
    """Office branch model."""
    objects = BranchQuerySet.as_manager()

    name = models.CharField(max_length=100, unique=True, default=None)
    facade_image = models.ImageField(upload_to='facades', max_length=100)
    location = PointField(srid=4326, db_index=True)

    class Meta:
        verbose_name_plural = 'branches'
        ordering = ['name']
        permissions = [
            ('can_edit_location', 'Can edit branch location'),
        ]

    def __str__(self):
        return self.name

    @property
    def longitude(self):
        """Location longitude."""
        return self.location.x

    @property
    def latitude(self):
        """Location latitude."""
        return self.location.y


class Employee(models.Model):
    """Branch employee model."""
    first_name = models.CharField(max_length=100, default=None)
    last_name = models.CharField(max_length=100, default=None)
    position = models.CharField(max_length=100, default=None)
    branch = models.ForeignKey(Branch, on_delete=models.CASCADE, related_name='employees')

    class Meta:
        ordering = ['last_name', 'first_name']

    def __str__(self):
        return f"{self.first_name} {self.last_name}"
