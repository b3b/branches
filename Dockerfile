FROM python:3.8

RUN apt-get update && apt-get install -y python-pip gdal-bin
RUN apt-get clean
RUN rm -rf /var/cache/apt/archives/* /var/lib/apt/lists/*

COPY requirements.txt /requirements.txt
RUN pip install -r /requirements.txt

ADD . /app
WORKDIR /app

CMD ["./run"]
